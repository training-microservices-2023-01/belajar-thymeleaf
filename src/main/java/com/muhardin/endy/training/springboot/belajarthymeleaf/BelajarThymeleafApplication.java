package com.muhardin.endy.training.springboot.belajarthymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.dialect.springdata.SpringDataDialect;

import nz.net.ultraq.thymeleaf.layoutdialect.LayoutDialect;

@SpringBootApplication
public class BelajarThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelajarThymeleafApplication.class, args);
	}

	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}

	@Bean
    public SpringDataDialect springDataDialect() {
        return new SpringDataDialect();
    }

}
