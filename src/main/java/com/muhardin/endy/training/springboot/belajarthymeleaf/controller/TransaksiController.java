package com.muhardin.endy.training.springboot.belajarthymeleaf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.muhardin.endy.training.springboot.belajarthymeleaf.dao.TransaksiDao;

@Controller
@RequestMapping("/transaksi")
public class TransaksiController {

    @Autowired private TransaksiDao transaksiDao;

    @GetMapping("/list")
    public ModelMap daftarTransaksi(Pageable page){
        return new ModelMap()
        .addAttribute("dataTransaksi", transaksiDao.findAll(page));
    }

    @GetMapping("/form")
    public void tampilkanFormTransaksi(){}
}
