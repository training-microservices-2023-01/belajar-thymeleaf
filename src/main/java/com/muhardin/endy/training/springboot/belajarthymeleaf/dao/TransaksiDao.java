package com.muhardin.endy.training.springboot.belajarthymeleaf.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.muhardin.endy.training.springboot.belajarthymeleaf.entity.Transaksi;

public interface TransaksiDao extends PagingAndSortingRepository<Transaksi, String> {
    
}
