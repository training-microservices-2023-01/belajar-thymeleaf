package com.muhardin.endy.training.springboot.belajarthymeleaf.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Entity @Data
public class Nasabah {

    @Id
    private String id;
    private String nomor;
    private String nama;
    private String email;
}
