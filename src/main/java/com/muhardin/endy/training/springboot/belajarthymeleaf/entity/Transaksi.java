package com.muhardin.endy.training.springboot.belajarthymeleaf.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Entity @Data
public class Transaksi {

    @Id
    private String id;

    @ManyToOne @JoinColumn(name="id_nasabah")
    private Nasabah nasabah;
    private LocalDateTime waktuTransaksi;
    private String keterangan;
    private BigDecimal nilai;
}
