create table nasabah (
    id VARCHAR(36),
    nomor VARCHAR(50) not null,
    nama VARCHAR(100),
    email VARCHAR(100),
    primary key (id),
    unique (nomor)
);

create table transaksi (
    id VARCHAR(36),
    id_nasabah VARCHAR(36) not null,
    keterangan VARCHAR(100) not null,
    waktu_transaksi timestamp not null,
    nilai decimal(19,2) not null,
    primary key (id),
    foreign key (id_nasabah) references nasabah(id)
);