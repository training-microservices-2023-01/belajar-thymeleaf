insert into nasabah (id, nomor, nama, email) values 
('n001', '001', 'Nasabah 001', 'n001@yopmail.com'),
('n002', '002', 'Nasabah 002', 'n002@yopmail.com'),
('n003', '003', 'Nasabah 003', 'n003@yopmail.com');

insert into transaksi (id, id_nasabah, keterangan, waktu_transaksi, nilai) values
('t001', 'n001', 'Setoran Tunai 001', '2023-01-01 09:00:00', 1000001),
('t002', 'n001', 'Setoran Tunai 002', '2023-01-02 09:00:00', 1000002),
('t003', 'n001', 'Setoran Tunai 003', '2023-01-03 09:00:00', 1000003),
('t004', 'n001', 'Setoran Tunai 004', '2023-01-04 09:00:00', 1000004),
('t005', 'n001', 'Setoran Tunai 005', '2023-01-05 09:00:00', 1000005),
('t006', 'n001', 'Setoran Tunai 006', '2023-01-06 09:00:00', 1000006),
('t007', 'n001', 'Setoran Tunai 007', '2023-01-07 09:00:00', 1000007),
('t008', 'n001', 'Setoran Tunai 008', '2023-01-08 09:00:00', 1000008),
('t009', 'n001', 'Setoran Tunai 009', '2023-01-09 09:00:00', 1000009),
('t010', 'n001', 'Setoran Tunai 010', '2023-02-01 09:00:00', 1000010),
('t011', 'n001', 'Setoran Tunai 011', '2023-02-01 09:00:00', 1000011),
('t012', 'n001', 'Setoran Tunai 012', '2023-02-01 09:00:00', 1000012);
